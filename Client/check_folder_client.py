#!/usr/bin/env python3
import os
import time
import socket
import sys
from time import gmtime, strftime
folder_file_info = []
folder_links = []
arg_list = sys.argv
folderLink = ''
HOST = ''
PORT = ''
userName = ''

if len(arg_list) >= 5:
	link_value_list = arg_list[1:-3]
	for value in link_value_list:
		folderLink+=value + ' '
	folderLink = folderLink[0:-1]
	try:
		HOST = arg_list[-3]
		PORT = int(arg_list[-2])
		userName = arg_list[-1]
	except:
		pass
elif len(arg_list) == 4:
	folderLink = arg_list[1]
	HOST = arg_list[2]
	PORT = int(arg_list[3])
	userName = arg_list[4]

if folderLink == '' or userName == '' or HOST == '' or PORT == '':
	print('Неполностю были написаны атрибуты для работы приложение!')
	print('Пожалуйста проверте данные и запустите еще раз!')

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
	time_now = strftime("%Y-%m-%d %H:%M:%S", gmtime())
	sock.connect((HOST, PORT))
	sock.sendall(bytes(time_now + ' Приложение запушенно у пользователя ' + userName + "\n", "utf-8"))

print("Приложение работает...")

def check_folder(folder_link):
	time_now = strftime("%Y-%m-%d %H:%M:%S", gmtime())
	is_exists = os.path.exists(folder_link)
	if is_exists:
		if folder_file_info == []:
			for dirname, dirnames, filenames in os.walk(folder_link):
				for subdirname in dirnames:
					link = os.path.join(dirname, subdirname)
					folder_size = os.path.getsize(link)
					folder_file_info.append({'link': link, 'size': str(folder_size)})
					folder_links.append(link)

				for filename in filenames:
					link = os.path.join(dirname, filename)
					file_size = os.path.getsize(link)
					folder_file_info.append({'link': dirname, 'size': str(file_size), 'file':filename})
					folder_links.append(link)
		elif folder_file_info != []:
			links = []
			for dirname, dirnames, filenames in os.walk(folder_link):
				for subdirname in dirnames:
					link = os.path.join(dirname, subdirname)
					if link not in folder_links:
						folder_links.append(link)
						folder_size = os.path.getsize(link)
						folder_file_info.append({'link': link, 'size': str(folder_size)})
						with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
							sock.connect((HOST, PORT))
							message = time_now + ' Пользователь ' + userName + ' создал новую папку: ' + link + ' | Размер: ' + str(folder_size) + 'Kb'
							sock.sendall(bytes(message + "\n", "utf-8"))
						links.append(link)
					else:
						links.append(link)

				for filename in filenames:
					link = os.path.join(dirname, filename)
					file_size = os.path.getsize(link)
					if link not in folder_links:
						folder_links.append(link)
						folder_file_info.append({'link': dirname, 'size': str(file_size), 'file':filename})
						with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
							sock.connect((HOST, PORT))
							message = time_now + ' Пользователь ' + userName + ' создал новую файл: ' + link + ' | Размер: ' + str(file_size) + 'Kb'
							sock.sendall(bytes(message + "\n", "utf-8"))
						links.append(link)
					else:
						for folder_info in folder_file_info:
							if folder_info.get('link') == dirname and folder_info.get('file') == filename:
								old_size = int(folder_info.get('size'))
								if old_size != file_size:
									with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
										sock.connect((HOST, PORT))
										message = time_now + ' Пользователь ' + userName + ' изменил файл: ' + link + ' | Старый размер файла: ' + folder_info.get('size') + 'Kb' + ' | Нынешный размер файла: ' + str(file_size) + 'Kb'
										sock.sendall(message.encode())
									folder_info['size'] = str(file_size)
						links.append(link)

			for file_name in folder_links:
				if file_name not in links:
					for folder in folder_file_info:
						if 'file' in folder:
							link_join = os.path.join(folder.get('link'), folder.get('file'))
							if link_join == file_name:
								with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
										sock.connect((HOST, PORT))
										message = time_now + ' Пользователь ' + userName + ' удалил файл: ' + link_join + ' | Размер: ' + folder.get('size') + 'Kb'
										sock.sendall(message.encode())
								elment_index = folder_file_info.index(folder)
								del folder_file_info[elment_index]
						else:
							if folder.get('link') == file_name:
								with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
										sock.connect((HOST, PORT))
										message = time_now + ' Пользователь ' + userName + ' удалил папку: ' + folder.get('link') + ' | Размер: ' + folder.get('size') + 'kb'
										sock.sendall(message.encode())
								elment_index = folder_file_info.index(folder)
								del folder_file_info[elment_index]
					
					elment_index = folder_links.index(file_name)
					del folder_links[elment_index]
				else:
					pass
	else:
		print ("Не корректный путь!")
		with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
			sock.connect((HOST, PORT))
			sock.sendall(bytes('Приложение остановлено из за не коректного пути' + "\n", "utf-8"))
		exit()

while True:
	check_folder(folderLink)
	time.sleep(2)


