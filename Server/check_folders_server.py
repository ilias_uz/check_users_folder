#!/usr/bin/env python3
import socketserver
import os
from conf_server import serverPort as PORT, folderForSaveLogsText
HOST = '0.0.0.0'
class MyTCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        self.data = self.request.recv(1024).strip()
        print (self.data.decode())
        file_link = folderForSaveLogsText+'/CheckFolderLog.txt'
        if os.path.isfile(file_link):
            with open(file_link, "r+") as f:
                old = f.read() # read everything in the file
                f.seek(0) # rewind
                f.write(old + "\n" + self.data.decode()) # write the new line before
        else:
            log_file = open(file_link, 'w+')
            log_file.write(self.data.decode())

if __name__ == "__main__":
    # Create the server, binding to localhost on port 9999
    server = socketserver.TCPServer((HOST, PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()